<?php

class NodeWidgets {
  private $widget_class = 'NodeWidgets';
  
  public function widgets() {
    $widgets = array();

    $widgets['node_add'] = array(
        'title' => t('Add content'),
        'group' => 'Dashboard node widgets',
        'data' => array(
            'plugin' => 'node',
            'callback' => 'widget_node_add',
            'arguments' => array(),
        ),
    );
    return $widgets;
  }

  public function widget_node_add() {
    $output = '';
    $item = menu_get_item('node/add');
    $types = system_admin_menu_block($item);

    if ($types) {
      $output = '<dl class="node-type-list">';
      foreach ($types as $item) {
        $output .= '<dt>' . l($item['title'], $item['href'], $item['localized_options']) . '</dt>';
        $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
      }
      $output .= '</dl>';
    } else {
      $output = '<p>' . t('You have not created any content types yet. Go to the <a href="@create-content">content type creation page</a> to add a new content type.', array('@create-content' => url('admin/structure/types/add'))) . '</p>';
    }

    return array(
        'title' => t('Create content'),
        'description' => t('Select your content type'),
        'content' => $output,
    );
  }

  public function views() {
    $view = new view;
    $view->name = 'dashboards_node_widgets';
    $view->description = 'Dashboards node views';
    $view->tag = 'Dashboards';
    $view->view_php = '';
    $view->base_table = 'node';
    $view->is_cacheable = FALSE;
    $view->api_version = 2;
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
    $handler = $view->new_display('default', 'Default', 'default');
    $handler->override_option('fields', array(
        'title' => array(
            'label' => 'Title',
            'alter' => array(
                'alter_text' => 0,
                'text' => '',
                'make_link' => 0,
                'path' => '',
                'link_class' => '',
                'alt' => '',
                'prefix' => '',
                'suffix' => '',
                'target' => '',
                'help' => '',
                'trim' => 0,
                'max_length' => '',
                'word_boundary' => 1,
                'ellipsis' => 1,
                'html' => 0,
                'strip_tags' => 0,
            ),
            'empty' => '',
            'hide_empty' => 0,
            'empty_zero' => 0,
            'link_to_node' => 1,
            'exclude' => 0,
            'id' => 'title',
            'table' => 'node',
            'field' => 'title',
            'relationship' => 'none',
        ),
        'status' => array(
            'id' => 'status',
            'table' => 'node',
            'field' => 'status',
        ),
        'edit_node' => array(
            'label' => 'Actions',
            'alter' => array(
                'alter_text' => 0,
                'text' => '',
                'make_link' => 0,
                'path' => '',
                'link_class' => '',
                'alt' => '',
                'prefix' => '',
                'suffix' => '',
                'target' => '',
                'help' => '',
                'trim' => 0,
                'max_length' => '',
                'word_boundary' => 1,
                'ellipsis' => 1,
                'html' => 0,
                'strip_tags' => 0,
            ),
            'empty' => '',
            'hide_empty' => 0,
            'empty_zero' => 0,
            'text' => '',
            'exclude' => 0,
            'id' => 'edit_node',
            'table' => 'node',
            'field' => 'edit_node',
            'relationship' => 'none',
        ),
    ));
    $handler->override_option('sorts', array(
        'changed' => array(
            'order' => 'DESC',
            'granularity' => 'second',
            'id' => 'changed',
            'table' => 'node',
            'field' => 'changed',
            'override' => array(
                'button' => 'Overwrite',
            ),
            'relationship' => 'none',
        ),
    ));
    $handler->override_option('filters', array(
        'sticky' => array(
            'id' => 'sticky',
            'table' => 'node',
            'field' => 'sticky',
        ),
        'promote' => array(
            'id' => 'promote',
            'table' => 'node',
            'field' => 'promote',
        ),
    ));
    $handler->override_option('access', array(
        'type' => 'none',
    ));
    $handler->override_option('cache', array(
        'type' => 'none',
    ));
    $handler->override_option('title', 'Content');
    $handler->override_option('use_ajax', TRUE);
    $handler->override_option('items_per_page', 15);
    $handler->override_option('use_pager', 'mini');
    $handler->override_option('style_plugin', 'table');
    $handler->override_option('style_options', array(
        'grouping' => '',
        'override' => 1,
        'sticky' => 0,
        'order' => 'asc',
        'columns' => array(
            'title' => 'title',
            'language' => 'language',
            'unpublish_on' => 'unpublish_on',
            'unpublish_on_1' => 'unpublish_on',
            'edit_node' => 'edit_node',
        ),
        'info' => array(
            'title' => array(
                'sortable' => 1,
                'separator' => ' ',
            ),
            'language' => array(
                'sortable' => 0,
                'separator' => '',
            ),
            'unpublish_on' => array(
                'sortable' => 0,
                'separator' => ' - ',
            ),
            'unpublish_on_1' => array(
                'sortable' => 0,
                'separator' => '',
            ),
            'edit_node' => array(
                'separator' => ' ',
            ),
        ),
        'default' => '-1',
    ));
    $handler = $view->new_display('dashboard_widget', 'Latest content', 'dashboard_widget_1');
    $handler->override_option('filters', array(
        'title' => array(
            'operator' => '=',
            'value' => '',
            'group' => '0',
            'exposed' => TRUE,
            'expose' => array(
                'use_operator' => 0,
                'operator' => 'title_op',
                'identifier' => 'title',
                'label' => 'Title',
                'optional' => 1,
                'remember' => 1,
            ),
            'case' => 1,
            'id' => 'title',
            'table' => 'node',
            'field' => 'title',
            'override' => array(
                'button' => 'Use default',
            ),
            'relationship' => 'none',
        ),
        'type' => array(
            'operator' => 'in',
            'value' => array(),
            'group' => '0',
            'exposed' => TRUE,
            'expose' => array(
                'use_operator' => 0,
                'operator' => 'type_op',
                'identifier' => 'type',
                'label' => 'Type',
                'optional' => 1,
                'single' => 1,
                'remember' => 1,
                'reduce' => 0,
            ),
            'id' => 'type',
            'table' => 'node',
            'field' => 'type',
            'override' => array(
                'button' => 'Use default',
            ),
            'relationship' => 'none',
        ),
    ));
    $handler->override_option('title', 'Latest content');
    $handler->override_option('style_options', array(
        'grouping' => '',
        'override' => 1,
        'sticky' => 0,
        'order' => 'asc',
        'columns' => array(
            'title' => 'title',
            'status' => 'status',
            'edit_node' => 'edit_node',
        ),
        'info' => array(
            'title' => array(
                'sortable' => 1,
                'separator' => ' ',
            ),
            'status' => array(
                'sortable' => 1,
                'separator' => '',
            ),
            'edit_node' => array(
                'separator' => ' ',
            ),
        ),
        'default' => '-1',
    ));
    $handler = $view->new_display('dashboard_widget', 'Sticky content', 'dashboard_widget_2');
    $handler->override_option('filters', array(
        'sticky' => array(
            'operator' => '=',
            'value' => '1',
            'group' => '0',
            'exposed' => TRUE,
            'expose' => array(
                'operator' => '',
                'identifier' => 'sticky',
                'label' => 'Sticky',
                'optional' => 0,
                'remember' => 0,
            ),
            'id' => 'sticky',
            'table' => 'node',
            'field' => 'sticky',
            'override' => array(
                'button' => 'Use default',
            ),
            'relationship' => 'none',
        ),
    ));
    $handler->override_option('title', 'Sticky content');
    $handler->override_option('style_options', array(
        'grouping' => '',
        'override' => 1,
        'sticky' => 0,
        'order' => 'asc',
        'columns' => array(
            'title' => 'title',
            'status' => 'status',
            'edit_node' => 'edit_node',
        ),
        'info' => array(
            'title' => array(
                'sortable' => 1,
                'separator' => ' ',
            ),
            'status' => array(
                'sortable' => 1,
                'separator' => '',
            ),
            'edit_node' => array(
                'separator' => ' ',
            ),
        ),
        'default' => '-1',
    ));
    $handler = $view->new_display('dashboard_widget', 'Frontpage content', 'dashboard_widget_3');
    $handler->override_option('filters', array(
        'promote' => array(
            'operator' => '=',
            'value' => '1',
            'group' => '0',
            'exposed' => TRUE,
            'expose' => array(
                'operator' => '',
                'identifier' => 'promote',
                'label' => 'Promoted to front page',
                'optional' => 1,
                'remember' => 1,
            ),
            'id' => 'promote',
            'table' => 'node',
            'field' => 'promote',
            'override' => array(
                'button' => 'Use default',
            ),
            'relationship' => 'none',
        ),
    ));
    $handler->override_option('title', 'Frontpage content');
    $handler->override_option('style_options', array(
        'grouping' => '',
        'override' => 1,
        'sticky' => 0,
        'order' => 'asc',
        'columns' => array(
            'title' => 'title',
            'status' => 'status',
            'edit_node' => 'edit_node',
        ),
        'info' => array(
            'title' => array(
                'sortable' => 1,
                'separator' => ' ',
            ),
            'status' => array(
                'sortable' => 1,
                'separator' => '',
            ),
            'edit_node' => array(
                'separator' => ' ',
            ),
        ),
        'default' => '-1',
    ));

    $views[$view->name] = $view;
    return $views;
  }

}
