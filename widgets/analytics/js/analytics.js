(function ($) {
  
  Drupal.behaviors.nice_dash_ga_widgets = {
    attach: function(context) {
      var script = document.createElement("script");
      script.src = "http://www.google.com/jsapi?callback=initGoogleDependencies";
      script.type = "text/javascript";
      document.getElementsByTagName("head")[0].appendChild(script);
    }
  }
  
  Drupal.behaviors.nice_dash = {
    attach: function(context) {

      window.initGoogleDependencies = function() {
        google.load('visualization', '1', {
          'callback':initGaWidgets, 
          'packages':['corechart','imagesparkline','geomap','table']
        })
      }
      window.initGaWidgets = function() {  
        if (Drupal.settings.dashboards.analytics.length > 0) {
          for (var i=0; i <= Drupal.settings.dashboards.analytics.length; i++) {
            if (Drupal.settings.dashboards.analytics[i] != undefined) {
              window['ga_get_' + Drupal.settings.dashboards.analytics[i]]();
            }      
          }
        }
      }

      window.ga_get_visits = function() {
        $.getJSON(Drupal.settings.basepath + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'visits'
        },function(result) {
          var options = {};
          options.width = $('#stats-visits').width() + 100;
          options.height = 200;
          options.legend = 'top';
          options.pointSize = 3;
          options.colors = ['#0077cc','e6f2fa'];
          options.hAxis = {
            slantedTextAngle:45, 
            slantedText:true, 
            textStyle: {
              fontSize: 11
            }
          };

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'x');
          data.addColumn('number', 'Pageviews');
          data.addColumn('number', 'Visits');

          for (var i = 0; i < result.data['results'].length; i++) {
            data.addRow([result.data['results'][i]['date'], result.data['results'][i]['pageviews'], result.data['results'][i]['visits']]);
          }

          // Create and draw the visualization.
          var chart = new google.visualization.LineChart(document.getElementById('stats-visits'));
          chart.draw(data, options);
        });
      }
  

      window.ga_get_visitors = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'visits'
        },function(result) {

          var options = {};
          options.cht = 'ls';
          options.colors = ['#0077cc'];
          options.chm='B,e6f2fa,0,0,0';
          options.fill = true;
          options.chls=2;
          options.width=220
          options.height=40
          options.showValueLabels= false;

          var data = new google.visualization.DataTable();

          data.addColumn('number');

          data.addRows(result.data['results'].length);

          for (var i = 0; i < result.data['results'].length; i++) {
            data.setValue(i, 0, result.data['results'][i]['visits']);
          }

          var chart = new google.visualization.ImageChart(document.getElementById('stats-visitors'));
          chart.draw(data, options);

          $('#stats-visitors').wrap('<div />');
          $('#stats-visitors').parent('div').append('<div class="float-left"><p>'+ Drupal.t('Visitors') +'</p><h3 class="ga-heading">'+ result.data['totalVisits'] +'</h3></div>');
        });
      }

      window.ga_get_pageviews = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'pageviews'
        },function(result) {

          var options = {};
          options.cht = 'ls';
          options.colors = ['#0077cc'];
          options.chm='B,e6f2fa,0,0,0';
          options.fill = true;
          options.chls=2;
          options.width=220
          options.height=40
          options.showValueLabels= false;

          var data = new google.visualization.DataTable();

          data.addColumn('number');

          data.addRows(result.data['results'].length);

          for (var i = 0; i < result.data['results'].length; i++) {
            data.setValue(i, 0, result.data['results'][i]['pageviews']);
          }

          var chart = new google.visualization.ImageChart(document.getElementById('stats-pageviews'));
          chart.draw(data, options);

          $('#stats-pageviews').wrap('<div />');
          $('#stats-pageviews').parent('div').append('<div class="float-left"><p>'+ Drupal.t('Pageviews') +'</p><h3 class="ga-heading">'+ result.data['totalPageViews'] +'</h3></div>');
        });
      }

      window.ga_get_bounces = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments : 'bounces'
        },function(result) {

          var options = {};
          options.cht = 'ls';
          options.colors = ['#0077cc'];
          options.chm='B,e6f2fa,0,0,0';
          options.fill = true;
          options.chls=2;
          options.width=220
          options.height=40
          options.showValueLabels= false;

          var data = new google.visualization.DataTable();
          data.addColumn('number');
          data.addRows(result.data['results'].length);

          for (var i = 0; i < result.data['results'].length; i++) {
            data.setValue(i, 0, result.data['results'][i]['rate']);
          }

          var chart = new google.visualization.ImageChart(document.getElementById('stats-bounces'));
          chart.draw(data, options);

          $('#stats-bounces').wrap('<div />');
          $('#stats-bounces').parent('div').append('<div class="float-left"><p>'+ Drupal.t('Bounce rate') +'</p><h3 class="ga-heading">'+ result.data['totalRate'] +'</h3></div>');
        });
      }

      window.ga_get_newvisits = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments : 'new_visits'
        },function(result) {

          var options = {};
          options.cht = 'ls';
          options.colors = ['#0077cc'];
          options.chm='B,e6f2fa,0,0,0';
          options.fill = true;
          options.chls=2;
          options.width=220
          options.height=40
          options.showValueLabels= false;

          var data = new google.visualization.DataTable();
          data.addColumn('number');
          data.addRows(result.data['results'].length);

          for (var i = 0; i < result.data['results'].length; i++) {
            data.setValue(i, 0, result.data['results'][i]['rate']);
          }

          var chart = new google.visualization.ImageChart(document.getElementById('stats-new-visits'));
          chart.draw(data, options);

          $('#stats-new-visits').wrap('<div class="center" />');
          $('#stats-new-visits').parent('div').append('<div class="float-left"><p>'+ Drupal.t('New visitors') +'</p><h3 class="ga-heading">'+ result.data['totalRate'] +'</h3></div>');
        });
      }


      window.ga_get_browser = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'browser'
        },function(result) {

          var options = {};
          options.width= $('#stats-browsers').width();
          options.height = $('#stats-browsers').width() - 150;
          options.is3D = true;

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Browser');
          data.addColumn('number', 'Visits');
          data.addRows(result.data.length);

          for (var i = 0; i < result.data.length; i++) {
            data.setValue(i, 0, result.data[i]['browser']);
            data.setValue(i, 1, result.data[i]['visits']);
          }

          var chart = new google.visualization.PieChart(document.getElementById('stats-browsers'));
          chart.draw(data, options);
        });
      }

      window.ga_get_geo = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'geo'
        },function(result) {

          var options = {};
          options.width= $('#stats-geo').width();
    
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Country');
          data.addColumn('number', 'Visits');
          data.addRows(result.data.length);

          for (var i = 0; i < result.data.length; i++) {
            data.setValue(i, 0, result.data[i]['country']);
            data.setValue(i, 1, result.data[i]['visits']);
          }

          var chart = new google.visualization.GeoMap(document.getElementById('stats-geo'));
          chart.draw(data, options);
        });
      }

      window.ga_get_pages = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'pages'
        },function(result) {

          var options = {};
          options.page = 'enable';
          options.pageSize = 10;

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Path');
          data.addColumn('number', 'Pageviews');
          data.addRows(result.data.length);

          for (var i = 0; i < result.data.length; i++) {
            data.setCell(i, 0, result.data[i]['pagepath']);
            data.setCell(i, 1,result.data[i]['pageviews']);
          }

          var chart = new google.visualization.Table(document.getElementById('stats-pages'));
          chart.draw(data, options);
        });
      }


      window.ga_get_entrances = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'entrances'
        },function(result) {

          var options = {};
          options.page = 'enable';
          options.pageSize = 10;

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Path');
          data.addColumn('number', 'Entrances');
          data.addRows(result.data.length);

          for (var i = 0; i < result.data.length; i++) {
            data.setCell(i, 0, result.data[i]['landingpagepath']);
            data.setCell(i, 1, result.data[i]['entrances']);
          }

          var chart = new google.visualization.Table(document.getElementById('stats-entrances'));
          chart.draw(data, options);
        });
      }

      window.ga_get_exits = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'exits'
        },function(result) {

          var options = {};
          options.page = 'enable';
          options.pageSize = 10;

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Path');
          data.addColumn('number', 'Exits');
          data.addRows(result.data.length);

          for (var i = 0; i < result.data.length; i++) {
            data.setCell(i, 0, result.data[i]['exitpagepath']);
            data.setCell(i, 1,result.data[i]['exits']);
          }

          var chart = new google.visualization.Table(document.getElementById('stats-exits'));
          chart.draw(data, options);
        });
      }
      window.ga_get_keywords = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'keywords'
        },function(result) {

          var options = {};
          options.page = 'enable';
          options.pageSize = 10;

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Keyword');
          data.addColumn('number', 'Total');
          data.addRows(result.data.length);

          for (var i = 0; i < result.data.length; i++) {
            data.setCell(i, 0, result.data[i]['keyword']);
            data.setCell(i, 1,result.data[i]['visits']);
          }

          var chart = new google.visualization.Table(document.getElementById('stats-keywords'));
          chart.draw(data, options);
        });
      }
      window.ga_get_sources = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'sources'
        },function(result) {

          var options = {};
          options.page = 'enable';
          options.pageSize = 10;

          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Source');
          data.addColumn('number', 'Total');
          data.addRows(result.data.length);

          for (var i = 0; i < result.data.length; i++) {
            data.setCell(i, 0, result.data[i]['source']);
            data.setCell(i, 1,result.data[i]['visits']);
          }

          var chart = new google.visualization.Table(document.getElementById('stats-sources'));
          chart.draw(data, options);
        });
      }
      window.ga_get_hours = function() {
        $.getJSON(Drupal.settings['basePath'] + 'ajax/dashboards',{
          action: 'widget',
          widget: 'analytics',
          callback: 'router',
          arguments: 'hours'
        },function(result) {

          var options = {};
          options.width = $('#stats-hours').width() + 100;
          options.height = 200;
          options.legend = 'none';
          options.hAxis = {
            slantedTextAngle:45, 
            slantedText:true, 
            textStyle: {
              fontSize: 9
            }
          };


          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Hour');
          data.addColumn('number', 'Total');

          data.addRows(result.data.length );

          for (var i = 0; i < result.data.length; i++) {
            data.setValue(i, 0, result.data[i]['hour']);
            data.setValue(i, 1, result.data[i]['visits']);
          }

          new google.visualization.ColumnChart(document.getElementById('stats-hours')).
          draw(data, options);
        });
      }  


    }
  }
})(jQuery);
