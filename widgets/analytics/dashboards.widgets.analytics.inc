<?php

class AnalyticsWidgets {
  private $widget_class = 'AnalyticsWidgets';
  
  public function widgets() {
    $widgets = array();
    
    $widgets['analytics_overview'] = array(
        'title' => t('Analytics overview'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_overview',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_browsers'] = array(
        'title' => t('Browsers'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_browsers',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_visits'] = array(
        'title' => t('Visitors'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_visits',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_map'] = array(
        'title' => t('Map'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_map',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_pages'] = array(
        'title' => t('Pages'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_pages',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_entrances'] = array(
        'title' => t('Entrances'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_entrances',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_exits'] = array(
        'title' => t('Exits'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_exits',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_sources'] = array(
        'title' => t('Sources'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_sources',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_keywords'] = array(
        'title' => t('Keywords'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_keywords',
            'arguments' => array(),
        ),
    );
    $widgets['analytics_hours'] = array(
        'title' => t('Hours'),
        'group' => 'Dashboard analytics widgets',
        'data' => array(
            'plugin' => 'analytics',
            'callback' => 'analytics_hours',
            'arguments' => array(),
        ),
    );
   
    return $widgets;
  }
  
  function router($type) {
    module_load_include('inc', 'dashboards', '/widgets/analytics/classes/gapi.class');

    switch ($type) {
      case 'visits':
        $data = $this->analytics_get_stats_visits();
        break;
      case 'pageviews':
        $data = $this->analytics_get_stats_pageviews();
        break;
      case 'bounces':
        $data = $this->analytics_get_stats_bounces();
        break;
      case 'new_visits':
        $data = $this->analytics_get_stats_new_visits();
        break;
      case 'browser':
        $data = $this->analytics_get_stats_browsers();
        break;
      case 'geo':
        $data = $this->analytics_get_stats_geo();
        break;
      case 'pages':
        $data = $this->analytics_get_stats_pages();
        break;
      case 'entrances':
        $data = $this->analytics_get_stats_entrances();
        break;
      case 'exits':
        $data = $this->analytics_get_stats_exits();
        break;
      case 'sources':
        $data = $this->analytics_get_stats_sources();
        break;
      case 'mediums':
        $data = $this->analytics_get_stats_mediums();
        break;
      case 'keywords':
        $data = $this->analytics_get_stats_keywords();
        break;
      case 'hours':
        $data = $this->analytics_get_stats_hours();
        break;
      default: $data = FALSE;
    }

    return drupal_json_output(array('status' => 1, 'data' => $data));
  }
  
  function analytics_get_stats_visits() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(variable_get('dashboards_google_account',  ''),  array('date'), array('visits', 'pageviews'), array('date'));
    $data = array();

    foreach  ($ga->getResults() as $result) {
      $date = $result->getDate();
      $date = mktime(0, 0, 0, drupal_substr($date, 4, 2), drupal_substr($date, 6, 2), drupal_substr($date, 0, 4));
      $date = format_date($date, 'custom', 'j M');

      $data['results'][] = array(
        'date' => $date,
        'visits' => $result->getVisits(),
        'pageviews' => $result->getPageViews(),
      );
    }

    $data['totalVisits'] = $ga->getVisits();

    return $data;
  }
  
  function analytics_get_stats_pageviews() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(variable_get('dashboards_google_account', ''), array('date'), array('pageviews'), array('date'));
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data['results'][] = array(
        'date' => $result->getDate(),
        'pageviews' => $result->getPageviews()
      );
    }

    $data['totalResults'] = $ga->getTotalResults();
    $data['totalPageViews'] = $ga->getPageviews();
    $data['updatedDate'] = $ga->getUpdated();

    return $data;
  }
  
  function analytics_get_stats_bounces() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(variable_get('dashboards_google_account', ''), array('date'), array('bounces', 'visits'), array('date'));
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data['results'][] = array(
        'day' => $result->getDate(),
        'bounces' => $result->getBounces(),
        'visits' => $result->getVisits(),
        'rate' => ($result->getBounces() / $result->getVisits()) * 100,
      );
    }
    $data['totalRate'] = round(($ga->getBounces() / $ga->getVisits()) * 100, 2) . '%';
    return $data;
  }
    
  function analytics_get_stats_new_visits() {
      $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(variable_get('dashboards_google_account', ''), array('date'), array('visits', 'newVisits'), array('date'));
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data['results'][] = array(
        'day' => $result->getDate(),
        'rate' => ($result->getNewVisits() / $result->getVisits()) * 100,
      );
    }
    $data['totalRate'] = round(($ga->getNewVisits() / $ga->getVisits()) * 100, 2) . '%';
    return $data;
  }
  
  
  function analytics_get_stats_browsers() {
     $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));
     $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('browser',  'browserversion'), // Dimensions
      array('visits'), // Metrics
      array('-visits'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      10 // Limit results
    );
    $data = array();

    foreach  ($ga->getResults() as $result) {
      $data[] = array(
        'browser' => $result->getBrowser() . ' ' . $result->getBrowserVersion(),
        'visits' => $result->getVisits()
      );
    }

    return $data;
  }
  
  function analytics_get_stats_geo() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('country'), // Dimensions
      array('pageviews', 'visits'), // Metrics
      array('-visits'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    foreach  ( $ga->getResults() as $result ) {
      $data[] = array(
        'country' => $result->getCountry(),
        'visits' => $result->getVisits()
      );
    }

    return $data;
  }
  
  function analytics_get_stats_pages() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('pagepath'), // Dimensions
      array('pageviews'), // Metrics
      array('-pageviews'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data[] = array(
        'pagepath' => $result->getPagePath(),
        'pageviews' => $result->getPageViews()
      );
    }

    return $data;
  }
  
  function analytics_get_stats_entrances() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('landingpagepath'), // Dimensions
      array('entrances'), // Metrics
      array('-entrances'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data[] = array(
        'entrances' => $result->getEntrances(),
        'landingpagepath' => $result->getLandingPagePath()
      );
    }

    return $data;
  }
  
  function analytics_get_stats_exits() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('exitpagepath'), // Dimensions
      array('exits'), // Metrics
      array('-exits'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data[] = array(
        'exits' => $result->getExits(),
        'exitpagepath' => $result->getExitPagePath()
      );
    }

    return $data;
  }
  
  function analytics_get_stats_sources() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('source'), // Dimensions
      array('visits'), // Metrics
      array('-visits'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data[] = array(
        'visits' => $result->getVisits(),
        'source' => $result->getSource()
      );
    }

    return $data;
  }
  
  function analytics_get_stats_mediums() {
    $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('medium'), // Dimensions
      array('visits'), // Metrics
      array('-visits'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    $labels = array(
      '(none)' => t('Direct traffic'),
      'referral' => t('Referring sites'),
      'organic' => t('Search engines'),
    );

    foreach ($ga->getResults() as $result) {
      $data[] = array(
        'medium' => $labels[$result->getMedium()],
        'visits' => $result->getVisits()
      );
    }

    return $data;
  }
  
  function analytics_get_stats_keywords() {
      $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('keyword'), // Dimensions
      array('visits'), // Metrics
      array('-visits'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data[] = array(
        'visits' => $result->getVisits(),
        'keyword' => $result->getKeyword()
      );
    }

    return $data;
  }
    
  function analytics_get_stats_hours() {
      $ga = new gapi(NULL, NULL, variable_get('dashboards_google_token', ''));

    $ga->requestReportData(
      variable_get('dashboards_google_account', ''),
      array('hour'), // Dimensions
      array('visits'), // Metrics
      array('-hour'), // Sort
      NULL, // Filter
      NULL, // Start date
      NULL, // End date
      1, // Start index
      NULL // Limit results
    );
    $data = array();

    foreach ($ga->getResults() as $result) {
      $data[] = array(
        'visits' => $result->getVisits(),
        'hour' => $result->getHour() . ':00'
      );
    }

    return $data;
  }
 
  function analytics_overview() {
    $this->analytics_add_assets();
    $this->analytics_register(array('visitors', 'pageviews', 'bounces', 'newvisits'));
    $output = '';

    $output .= '<div id="stats-visitors" class="float-left overview"></div>';
    $output .= '<br class="clear-both" />';
    $output .= '<div id="stats-pageviews" class="float-left overview"></div>';
    $output .= '<br class="clear-both" />';
    $output .= '<div id="stats-bounces" class="float-left overview"></div>';
    $output .= '<br class="clear-both" />';
    $output .= '<div id="stats-new-visits" class="float-left overview"></div>';
    $output .= '<br class="clear-both" />';

    return array(
        'title' => t('Analytics overview'),
        'description' => t('Summary of useful statistics.'),
        'content' => '<div id="ga-overview">' . $output . '</div>',
    );
  }
  
  function analytics_browsers() {
   $this->analytics_add_assets();
    $this->analytics_register(array('browser'));
    $output = '';

    $output .= '<div class="stats-wrapper"><div id="stats-browsers"></div></div>';

    return array(
        'title' => t('Browser statistics'),
        'description' => t('Top 10 of browsers visitors are using to view this website.'),
        'content' => $output,
    );
  }

  /**
   * Google analytics.
   */
  function analytics_visits() {
   $this->analytics_add_assets();
    $this->analytics_register(array('visits'));
    $output = '';

    $output .= '<div class="stats-wrapper"><div id="stats-visits"></div></div>';

    return array(
        'title' => t('Visitor statistics'),
        'description' => t('Overview of visits and pageviews from the last month'),
        'content' => $output,
    );
  }

  function analytics_map() {
   $this->analytics_add_assets();
    $this->analytics_register(array('geo'));
    $output = '';

    $output .= '<div id="stats-geo"></div>';

    return array(
        'title' => t('Geographical statistics'),
        'description' => t('Geographical overview of site visitors'),
        'content' => $output,
    );
  }
 
  function analytics_pages() {
   $this->analytics_add_assets();
    $this->analytics_register(array('pages'));
    $output = '';

    $output .= '<div id="stats-pages"></div>';

    return array(
        'title' => t('Page statistics'),
        'description' => t('Overview of per page statistics.'),
        'content' => $output,
    );
  }

  function analytics_entrances() {
   $this->analytics_add_assets();
    $this->analytics_register(array('entrances'));
    $output = '';

    $output .= '<div id="stats-entrances"></div>';

    return array(
        'title' => t('Entrance statistics'),
        'description' => t('Overview of pages on which visitors enter your site.'),
        'content' => $output,
    );
  }

  function analytics_exits() {
   $this->analytics_add_assets();
    $this->analytics_register(array('exits'));
    $output = '';

    $output .= '<div id="stats-exits"></div>';

    return array(
        'title' => t('Exit statistics'),
        'description' => t('Overview of pages on which visitors exit your site.'),
        'content' => $output,
    );
  }

  function analytics_sources() {
   $this->analytics_add_assets();
    $this->analytics_register(array('sources'));
    $output = '';

    $output .= '<div id="stats-sources"></div>';

    return array(
        'title' => t('Source statistics'),
        'description' => t('Overview of which sites direct traffic to your site.'),
        'content' => $output,
    );
  }

  function analytics_keywords() {
   $this->analytics_add_assets();
    $this->analytics_register(array('keywords'));
    $output = '';

    $output .= '<div id="stats-keywords"></div>';

    return array(
        'title' => t('Keyword statistics'),
        'description' => t('Overview of what keywords are used to find your website.'),
        'content' => $output,
    );
  }

  function analytics_hours() {
   $this->analytics_add_assets();
    $this->analytics_register(array('hours'));
    $output = '';

    $output .= '<div class="stats-wrapper"><div id="stats-hours"></div></div>';

    return array(
        'title' => t('Hour statistics'),
        'description' => t('Overview of site activity per hour.'),
        'content' => $output,
    );
  }

  public function settings_form(&$form, &$form_state) {
    $form = array();
   
    
    $form['google_analytics'] = array(
        '#type' => 'fieldset',
        '#title' => t('Google analytics settings'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );
    
    // If authenticated show form
    $token = variable_get('dashboards_google_token', '');
    
    
    if (!empty($token)) {
      module_load_include('inc', 'dashboards', '/widgets/analytics/classes/gapi.class');
     
      $ga = new gapi(NULL, NULL, $token);
      $ga->requestAccountData();
       
      $options = array();
      $options[] = t('No account');
      foreach ($ga->getResults() as $result) {
        $options[$result->getProfileId()] = check_plain($result);
      }

      if (!count($options)) {
        $form['google_analytics']['notice'] = array(
            '#value' => t("Currently you don't have any google analytics accounts")
        );
      }
      else {
        $form['google_analytics']['dashboards_google_account'] = array(
            '#type' => 'select',
            '#title' => t('Site account'),
            '#default_value' => variable_get('dashboards_google_account', ''),
            '#options' => $options,
            '#description' => t('Choose which account will be used to aggregate statistics.'),
        );
      }
    }
    else {
      // Capture token
      if (isset($_GET['token'])) {
        $headers = array('Authorization' => 'AuthSub token="' . $_GET['token'] . '"');
        $url = 'https://www.google.com/accounts/AuthSubSessionToken';

        $response = drupal_http_request($url, array('headers' => $headers));
        list($temp, $token) = explode('=', $response->data, 2);

        if (empty($token)) {
          watchdog('Dashboards', t('Failed to receive AuthSub session token.'));
          drupal_set_message(t('Failed to receive AuthSub session token.'), 'error');
        }
        else {
          variable_set('dashboards_google_token', $token);
          return drupal_goto('admin/structure/dashboards/settings');
        }
      }
      else {
        // Build google auth link
        $next_url = url('admin/structure/dashboards/settings', array('absolute' => TRUE));
        $url = 'https://www.google.com/accounts/AuthSubRequest?session=1';
        $url .= '&next=';
        $url .= urlencode($next_url);
        $url .= "&scope=";
        $url .= urlencode('https://www.google.com/analytics/feeds/');

        $output = '';
        $output .= '<p>' . t('You have to authenticate with Google first. Before you can use the widgets.') . '</p>';
        $output .= l(t('Begin authentication'), $url);

        $form['google_analytics']['google_auth'] = array(
            '#markup' => $output,
        );
      }
    }
  }
  function analytics_add_assets() {
    static $assets_added;

    if ($assets_added) {
      return;
    }
    else {
      drupal_add_js(drupal_get_path('module', 'dashboards') . '/widgets/analytics/js/analytics.js');
      drupal_add_css(drupal_get_path('module', 'dashboards') . '/widgets/analytics/css/analytics.css');
      $assets_added = TRUE;
    }
  }
  function analytics_register($widgets) {
    foreach ( (array) $widgets as $widget) {
      drupal_add_js(array('dashboards' => array('analytics' => array($widget))), 'setting');
    }
  }
}
