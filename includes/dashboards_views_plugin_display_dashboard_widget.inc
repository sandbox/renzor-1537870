<?php

/**
 * @file
 * Contains the dashboard widget display plugin.
 */

/**
 * A plugin to handle defaults on a view.
 *
 * @ingroup views_display_plugins
 */
class dashboards_views_plugin_display_dashboard_widget extends views_plugin_display {

  function execute() {
    return $this->view->render($this->display->id);
  }

  function execute_dashboard_widget() {
    return array(
      'id' => $this->view->name . '-' . $this->display->id,
      'title' => $this->display->display_title,
      'data' => array(
        'callback' => 'dashboards_views_embed',
        'arguments' => array($this->view->name, $this->display->id),
      ),
    );
  }
}