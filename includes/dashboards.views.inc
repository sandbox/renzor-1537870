<?php

/**
 * Implements hook_views_plugins().
 */
function dashboards_views_plugins() {
  return array(
    'display' => array(
      'dashboard_widget' => array(
        'title' => t('Dashboard widget'),
        'help' => t('Creates a dashboard display.'),
        'handler' => 'dashboards_views_plugin_display_dashboard_widget',
//        'theme' => 'dashboard_view_widget_display',
//        'theme path' => drupal_get_path('module', 'dashboards') . '/theme',
//        'theme file' => 'dashboards.theme.inc', 
           'theme' => 'views_view',
           'theme path' => drupal_get_path('module', 'views') . '/theme',
        'dashboard_widget' => TRUE,
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'use more' => FALSE,
        'uses options' => TRUE,
        'accept attachments' => TRUE,
        //'admin' => t('Dashboard widget'),
        //'help topic' => 'display-default',
        'path' => drupal_get_path('module', 'dashboards') . '/includes',
      ),
    ),
  );
}

/**
 * Implements hook_views_default_views().
 */
function dashboards_views_default_views() {
  $views = array();
  $widgets = dashboards_widget_plugins();  
  
  foreach ((array) $widgets as $widget_name => $widget) {
    $widget = dashboards_get_widget($widget_name);
    if (method_exists($widget, 'views') && is_array($widget->views())) {
      $views = array_merge($widget->views(), $views);
    }
  }
  return $views;
}

