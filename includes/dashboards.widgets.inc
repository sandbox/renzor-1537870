<?php

/**
 * Retrieves all dashboard widget provided by the dashboard module or other modules.
 */
function dashboards_widgets_type_custom() {
  $items = array();
  $widgets = dashboards_widget_plugins();

  foreach ((array) $widgets as $widget_name => $widget) {
    $widget = dashboards_get_widget($widget_name);

    if (method_exists($widget, 'widgets') && is_array($widget->widgets())) {
      $plugin_widgets = array();
      foreach ($widget->widgets() as $key => $value) {
        $plugin_widgets[$key] = $value + array(
            'type' => 'plugin',
            'id' => $key,
        );
      }
      $items = array_merge($items, $plugin_widgets);
    }
  }

  return $items;
}

/**
 * Retrieves all dashboard widget views. 
 */
function dashboards_widgets_type_view() {
  $items = array();
  $views = views_get_all_views();
  foreach ($views as $view) {

    if (!empty($view->disabled)) {
      continue;
    }

    $view->init_display();
    foreach ($view->display as $display_id => $display) {
      if (isset($view->disabled)) {
        if ($view->disabled == FALSE) {
          if (isset($display->display_plugin) && $display->display_plugin === 'dashboard_widget') {
            $result = $display->handler->execute_dashboard_widget();
            if (is_array($result)) {
              $result['type'] = 'view';
              $result['group'] = t('Dashboard views');
              $items[$result['id']] = $result;
            }
          }
        }
      }
    }
  }
  
  return $items;
}

/**
 * Retrieves all drupal blocks.
 */
function dashboards_widgets_type_block() {
  $items = array();
  
  // Enabled modules.
  $result = db_select('system', 'sys')
        ->fields('sys', array('name', 'info'))
        ->condition('type', 'module', '=')
        ->condition('status', 1, '=')          
        ->execute()
        ->fetchAll();

  $modules = array();
  foreach ($result as $row) {
    $info = unserialize($row->info);
    $modules[$row->name] = isset($info['name']) ? $info['name'] : $row->name;
  }
  
  // Get blocks.
  $block_info = array();
  foreach (module_implements('block_info') as $module) {
    $module_blocks = module_invoke($module, 'block_info');
    if ($module_blocks) {
      foreach ($module_blocks as $delta => $info) {
        $data = array(
            'title' => $info['info'],
            'id' => $module . '-' . $delta,
            'type' => 'block',
            'group' => $modules[$module],
            'data' => array(
                'callback' => 'module_invoke',
                'arguments' => array($module, 'block_view', $delta),
            ),
        );
        $items["{$module}-{$delta}"] = $data;
      }
    }
  }
  ksort($items);
  return $items;
}
