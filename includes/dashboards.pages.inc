<?php

/**
 * Callback: Dashboard page.
 */
function dashboard_page_view(Dashboard $dashboard) {
  return theme('dashboard_page', array('dashboard' => $dashboard));
}

/**
 * Callbak: Dashboard ajax. 
 */
function dashboard_ajax() {
  switch ($_REQUEST['action']) {
    case 'save':
        dashboard_widgets_save();
      break;
    case 'widget':
        dashboard_widget_action();
      break;
    default: return;
  }  
}

function dashboard_widgets_save() {
  if (isset($_REQUEST['name']) && isset($_REQUEST['wid']) && isset($_REQUEST['region'])) {
    $dashboard = dashboard_load($_REQUEST['name']);
    if (!$dashboard) {
      return;
    }
    
    // Sanitize;
    $wid = check_plain($_REQUEST['wid']);
    $region = check_plain($_REQUEST['region']);
    
    $dashboard->data[$_REQUEST['wid']] = array(
      'region' => $region,  
    );
    $dashboard->changed = time();
    
    dashboard_save($dashboard);    
  }
}

function dashboard_widget_action() {
   if (isset($_REQUEST['widget']) && isset($_REQUEST['callback']) && isset($_REQUEST['arguments'])) {
     $widget = dashboards_get_widget($_REQUEST['widget']);
     $callback = $_REQUEST['callback'];
     $arguments = $_REQUEST['arguments'];
     if (method_exists($widget, $callback)) {
       return $widget->$callback($arguments);
     }
   }
}