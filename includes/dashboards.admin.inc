<?php

/**
 * Generates the model type editing form.
 */
function dashboard_form($form, &$form_state, $dashboard, $op = 'edit') { 
  $form_state['dashboard'] = $dashboard;
  
  if ($op == 'clone') {
    $dashboard->title .= ' (cloned)';
    $dashboard->name = '';
  }

  $form['title'] = array(
      '#title' => t('Title'),
      '#type' => 'textfield',
      '#default_value' => $dashboard->title,
      '#description' => t('The human-readable name of this dashboard'),
      '#required' => TRUE,
      '#size' => 30,
  );

  $form['name'] = array(
      '#type' => 'machine_name',
      '#default_value' => $dashboard->name,
      '#maxlength' => 21,
      '#machine_name' => array(
          'exists' => 'dashboards_name_exists',
          'source' => array('title'),
      ),
  );

  $form['actions'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('form-actions')),
      '#weight' => 400,
  );

  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save dashboard'),
      '#submit' => $submit + array('dashboards_edit_form_submit'),
  );

  if (!empty($model->name)) {
    $form['actions']['delete'] = array(
        '#type' => 'submit',
        '#value' => t('Delete dashboard'),
        '#suffix' => l(t('Cancel'), 'admin/structure/dashboards'),
        '#submit' => $submit + array('dashboards_edit_form_submit_delete'),
        '#weight' => 45,
    );
  }

  return $form;
}

/**
 * Form API submit callback for the dashboard form.
 */
function dashboards_edit_form_submit(&$form, &$form_state) {
  $dashboard = entity_ui_controller('dashboard')->entityFormSubmitBuildEntity($form, $form_state);

  if (empty($dashboard->created)) {
    $dashboard->created = time();
  }

  $dashboard->changed = time();

  $dashboard->save();

  $form_state['redirect'] = 'admin/structure/dashboards';
}

/**
 * Form API submit callback for the delete button.
 * 
 * @todo Remove hard-coded path
 */
function dashboards_edit_form_submit_delete(&$form, &$form_state) {
  //$form_state['redirect'] = 'admin/content/models/model/' . $form_state['model']->model_id . '/delete';
}

/**
 * Validates if a given dashboard name exists.
 * @param type $name
 * @return type 
 */
function dashboards_name_exists($name) {
  $result = db_select('dashboards', 'd')
          ->fields('d', array('did'))
          ->condition('name', $name)
          ->execute()
          ->fetchField();

  if (!empty($result)) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Generates the model type editing form.
 */
function dashboards_settings_form($form_state) { 
  $form = array();
  $widgets = dashboards_widget_plugins();
  foreach ((array) $widgets as $widget_name => $widget) {
    $widget = dashboards_get_widget($widget_name);
    if (method_exists($widget, 'settings_form')) {
      $widget->settings_form($form, $form_state);      
    }
  }

  return system_settings_form($form);
}
