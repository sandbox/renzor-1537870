<?php

/**
 * Implements hook_menu().
 */
function dashboards_menu() {
  $items = array();
  
  $items['ajax/dashboards'] = array(
    'page callback' => 'dashboard_ajax',
    'access arguments' => array('administer dashboards'),
    'type' => MENU_CALLBACK,
    'file' => 'dashboards.pages.inc',
    'file path' => drupal_get_path('module', 'dashboards') . '/includes',
  );
    
  return $items;
}

/**
 * Implements hook_entity_info().
 */
function dashboards_entity_info() {
  $info = array();

  $info['dashboard'] = array(
      'label' => t('Dashboard'),
      'entity class' => 'Dashboard',
      'controller class' => 'DashboardsController',
      'base table' => 'dashboards',
      'uri callback' => 'entity_class_uri',
      'label callback' => 'dashboard_label',
      'exportable' => TRUE,
      'entity keys' => array(
          'id' => 'name',
          'name' => 'name',
      ),
      'static cache' => TRUE,
      'bundles' => array(
          'dashboard' => array(
              'label' => 'Dashboard',
          ),
      ),
      'module' => 'dashboards',
      'access callback' => 'dashboards_access',
      'admin ui' => array(
          'path' => 'admin/structure/dashboards',
          'file' => 'dashboards.admin.inc',
          'file path' => drupal_get_path('module', 'dashboards') . '/includes',
          'controller class' => 'DashboardsUIController',
          'menu wildcard' => '%dashboard',
      ),
  );

  return $info;
}

/**
 * Implements hook_permission().
 */
function dashboards_permission() {

  $permissions = array(
      'administer dashboards' => array(
          'title' => t('Administer dashboards'),
          'description' => t('Edit and delete all dashboards'),
      ),
  );

  return $permissions;
}

/**
 * Dashboard access control
 */
function dashboards_access($op, $dashboard = NULL, $account = NULL) {
  if (user_access('administer dashboards', $account)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implements hook_theme().
 */
function dashboards_theme($existing, $type, $theme, $path) {
  $items = array();
  $path = drupal_get_path('module', 'dashboards') . '/theme';
  
  $defaults = array(
      'file' => 'dashboards.theme.inc',
      'path' => $path,
  );

  $items['dashboard_page'] = array(
      'variables' => array('dashboard' => NULL),
  ) + $defaults;
  
  $items['dashboard_widget_filter'] = array(
      'variables' => array('widgets' => NULL),
  ) + $defaults;

  $items['dashboard_widget'] = array(
      'variables' => array('title' => NULL, 'description' => NULL, 'content' => NULL),
  ) + $defaults;

  $items['dashboard_widget_list_item'] = array(
      'variables' => array('title' => NULL, 'id' => NULL),
  ) + $defaults;
  
  $items['dashboard_layout'] = array(
      'variables' => array('setup' => NULL , 'regions' => NULL),
  ) + $defaults;

  return $items;
}

/**
 * Dashboard load function
 * @param type $did
 * @param type $reset
 * @return type 
 */
function dashboard_load($did = NULL) {
  $dids = (isset($did) ? array($did) : array());
  $dashboard = dashboards_load_multiple($dids);
  return $dashboard ? reset($dashboard) : FALSE;
}

/**
 * Dashboard load multiple.
 * @param type $dids
 * @param type $conditions
 * @param type $reset
 * @return type 
 */
function dashboards_load_multiple($dids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('dashboard', $dids, $conditions, $reset);
}

/**
 * Create a dashboard object.
 */
function dashboards_create($values = array()) {
  return entity_get_controller('dashboard')->create($values);
}

/**
 * Deletes a dashboard.
 */
function dashboard_delete(Dashboard $dashboard) {
  $dashboard->delete();
}

/**
 * Delete multiple dashboards.
 *
 * @param $dids
 *   An array of dashboard IDs.
 */
function dashboard_delete_multiple(array $dids) {
  entity_get_controller('dashboard')->delete($dids);
}

/**
 * Saves a dashboard to the database.
 *
 * @param $dashboard
 *   The dashboard object.
 */
function dashboard_save(Dashboard $dashboard) {
  return $dashboard->save();
}

/**
 * Label callback
 * @param type $dashboard
 * @return type 
 */
function dashboard_label($dashboard) {
  return $dashboard->title;
}

/**
 * Retrieve all available widgets.
 */
function dashboards_widgets() {
  $widgets = array();
  
  // @TODO implement cache layer.
  
  module_load_include('inc','dashboards', 'includes/dashboards.widgets');

  // View widgets
  $view_widgets = dashboards_widgets_type_view();
  $widgets = array_merge($widgets, $view_widgets);

  // Custom non-view widgets
  $custom_widgets = dashboards_widgets_type_custom();
  $widgets = array_merge($widgets, $custom_widgets);
  
  // All blocks  
  $blocks = dashboards_widgets_type_block();
  $widgets = array_merge($widgets, $blocks); 
  
  return $widgets;
}

/**
 * Implements hook_dashboards_plugins().
 */
function dashboards_dashboards_widgets() {
  $plugins = array();

  $path = drupal_get_path('module', 'dashboards') . '/widgets';

  $plugins['node'] = array(
      'handler' => array(
          'path' => $path . '/node',
          'file' => 'dashboards.widgets.node.inc',
          'class' => 'NodeWidgets',
      ),
  );
  $plugins['analytics'] = array(
      'handler' => array(
          'path' => $path . '/analytics',
          'file' => 'dashboards.widgets.analytics.inc',
          'class' => 'AnalyticsWidgets',
      ),
  );

  return $plugins;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function dashboards_ctools_plugin_api($owner, $api) {
  if ($owner == 'dashboards' && ($api == 'dashboards' || $api == 'widgets')) {
    return array('version' => 1);
  }
}

/**
 * Implements hook_ctools_plugin_type().
 */
function dashboards_ctools_plugin_type() {
  return array(
      'widgets' => array(
          'cache' => TRUE,
          'use hooks' => TRUE,
          'classes' => array('handler')
      ),
  );
}

/**
 * Implements hook_views_api().
 */
function dashboards_views_api() {
  return array(
      'api' => 3.0,
      'path' => drupal_get_path('module', 'dashboards') . '/includes',
  );
}

/**
 * Get all visible plugins or a plugin handler.
 *
 * @param string $widget_name
 *   The name of the plugin.
 * @return mixed
 *   Either a list of plugins or a loaded plugin class.
 */
function dashboards_get_widget($widget_name = NULL) {
  static $cache = array();
  static $cached_plugins = array();

  if (empty($cached_plugins)) {
    ctools_include('plugins');
    $plugins = ctools_get_plugins('dashboards', 'widgets');
    $cached_plugins = $plugins;
  }

  if ($widget_name == NULL) {
    return $cached_plugins;
  } else {
    if (!isset($cache[$widget_name])) {
      if ($class = ctools_plugin_get_class($cached_plugins[$widget_name], 'handler')) {
        $cache[$widget_name] = new $class($cached_plugins[$widget_name]);
      }
    }
    return isset($cache[$widget_name]) ? $cache[$widget_name] : FALSE;
  }
}

/**
 * Get all widgets.
 */
function dashboards_widget_plugins() {
  ctools_include('plugins');
  return ctools_get_plugins('dashboards', 'widgets');
}

/**
 * Render a widget.
 */
function dashboards_widget_render($widget) {
  $output = '';
  $data = $widget['data'];
  if (isset($data['callback'])) {
    if ($widget['type'] === 'plugin' && isset($data['plugin'])) {
      $obj = dashboards_get_widget($data['plugin']);
      $content = $obj->$data['callback']($data['arguments']);     
    }
    elseif ($widget['type'] === 'view') {
      $content = $data['callback']($data['arguments']);
    }
    elseif ($widget['type'] === 'block') {
      $content = $data['callback']($data['arguments'][0], $data['arguments'][1], $data['arguments'][2]);     
    }
  }
  if (is_array($content)) {
     $widget = array_merge($content, $widget);
  }
  return theme('dashboard_widget', $widget);
}

/**
 * Embed funcion.
 */
function dashboards_views_embed($arguments) {
  $view = views_get_view($arguments[0]);

  if (!$view || !$view->access($arguments[1]) || (isset($view->disabled) && $view->disabled !== FALSE)) {
    return;
  }

  return array(
      'title' => $view->display[$arguments[1]]->display_options['title'],
      //'description' => $description,
      'content' => $view->preview($arguments[1]),
  );
}

/**
 * The class used for dashboard entities
 */
class Dashboard extends Entity {

  public function __construct($values = array()) {
    parent::__construct($values, 'dashboard');
  }

  protected function defaultUri() {
    return array('path' => 'admin/dashboards/' . $this->name);
  }

  public function available_widgets() {
    return dashboards_widgets();
  }

}

/**
 * UI controller.
 */
class DashboardsUIController extends EntityDefaultUIController {
  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  // public function hook_menu() {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage dashboards.';

    $items['admin/dashboards'] = array(
        'title' => 'Dashboards',
        'page callback' => 'dashboard_page_view',
        'access callback' => 'dashboards_access',
        'access arguments' => array('view'),
        'type' => MENU_NORMAL_ITEM,
        'file' => 'dashboards.pages.inc',
        'file path' => drupal_get_path('module', 'dashboards') . '/includes',
    );

    $items['admin/dashboards/%dashboard'] = array(
        'title' => 'title',
        'page callback' => 'dashboard_page_view',
        'page arguments' => array(2),
        'access callback' => 'dashboards_access',
        'access arguments' => array('view', 2),
        'type' => MENU_CALLBACK,
        'file' => 'dashboards.pages.inc',
        'file path' => drupal_get_path('module', 'dashboards') . '/includes',
    );
    
    $items['admin/structure/dashboards/settings'] = array(
        'title' => 'Settings',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('dashboards_settings_form'),        
        'access arguments' => array('administer dashboards'),
        'type' => MENU_NORMAL_ITEM,
        'file' => 'dashboards.admin.inc',
        'file path' => drupal_get_path('module', 'dashboards') . '/includes',
    );
    
    return $items;
  }

}
