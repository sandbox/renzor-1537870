<?php

/**
 * Controller
 */
class DashboardsController extends EntityAPIController{
  
  /**
   * Create a dashboard - we first set up the values that are specific
   * to our model schema but then also go through the EntityAPIController
   * function.
   * 
   * @param $type
   *   The machine-readable type of the dashboard.
   *
   * @return
   *   A dashboard object with all default fields initialized.
   */
  public function create(array $values = array()) {
    // Add values that are specific to our Model
    $values += array( 
      'did' => '',
      'title' => '',
      'name' => '',
      'created' => '',
      'changed' => '',
      'data' => '',
      'type' => 'dashboard', // For now all dashboards are of type dashboard.
    );
    
    $dashboard = parent::create($values);
    return $dashboard;
  }
  
}
