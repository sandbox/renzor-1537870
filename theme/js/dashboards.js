(function ($) {
  Drupal.behaviors.dashboards = {
    dashboard: null,
    attach: function(context) {
      that = this;
      this.sortables();
      this.filters();
      this.dashboard = Drupal.settings.dashboards.dashboard_name;
    },
    filters: function() {
      $('.widget-group-wrapper .item-list').css('display','none');
      $('.widget-group-name').bind('click', function() {
        $('.widget-group-wrapper .item-list').css('display','none');
        $(this).next('.item-list').css('display', 'block');
      });
    },
    sortables: function() {
      $('.dashboard-column').sortable({
        connectWith: '.dashboard-column',
        handle: 'h2',
        cursor: 'move',
        placeholder: 'placeholder',
        forcePlaceholderSize: true,
        opacity: 0.4,
        stop: function(event, ui) {
          that.stopSortableHandler(event, ui);
        },
        start: function(event, ui) {
          that.startSortableHandler(event, ui);
        }
      })
      .disableSelection();
      $('.widget').each(function(){
        $(this).hover(function(){
          $(this).find('h2').addClass('collapse');
        }, function(){
          $(this).find('h2').removeClass('collapse');
        })
        .find('h2').hover(function(){
          $(this).find('.configure').css('visibility', 'visible');
        }, function(){
          $(this).find('.configure').css('visibility', 'hidden');
        })
        .click(function(){
          $(this).siblings('.dragbox-content').toggle();
        })
        .end()
        .find('.configure').css('visibility', 'hidden');
      });      
    },
    stopSortableHandler: function(event, ui) {
      $('.dashboard-column').css('border', 'none');
      $.getJSON(Drupal.settings.basePath + "ajax/dashboards", {
        action: 'save',
        name: this.dashboard,
        wid: $(ui.item).attr('data-id'),
        region: $(ui.item).parents('.dashboard-column').attr('data-id'),
        format: "json"
      },
      function(data) {
       //console.log(data);
      });    
    },
    startSortableHandler: function(event, ui) {
      $('.dashboard-column').css('border', '1px dashed #ddd');
    }
    
  }  
})(jQuery);
