<?php
/**
 * Theme: Dashboard page.
 */
function theme_dashboard_page($vars) {
  $output = '';
  $dashboard = $vars['dashboard'];
  $path = drupal_get_path('module','dashboards');
  
  drupal_add_css($path . '/theme/css/dashboards.css');
  drupal_add_js($path . '/theme/js/dashboards.js');
  
  drupal_add_library('system', 'ui.droppable');
  drupal_add_library('system', 'ui.sortable');  

  $js_settings = array(
      'dashboards' => array(
          'dashboard_name' => $dashboard->name,
      ),
  );
  
  drupal_add_js($js_settings, 'setting');
  
  $widgets = $vars['dashboard']->available_widgets();
  $output .= theme('dashboard_widget_filter', array('widgets' => $widgets));
  
  $region_data = array();
  foreach ((array) $dashboard->data as $id => $value) {
     if(isset($widgets[$id])) {
       if (!isset($region_data[$value['region']])) {
         $region_data[$value['region']] = '';
       }
       $region_data[$value['region']] .= dashboards_widget_render($widgets[$id]);
     }
  }
  
  $setup = array(
      'one-col',
      'two-col',
      'two-col',
      'one-col',
  );
  
  $output .= theme('dashboard_layout', array('setup' => $setup, 'regions' => $region_data));
  return $output; 
}

/**
 * Theme: Dashboard widget filter
 */
function theme_dashboard_widget_filter($vars) {
  $groups = array();
  foreach ((array) $vars['widgets'] as $id => $value) {
    if (isset($groups[$value['group']])) {
      $groups[$value['group']]['children'][] = array(
          'data' => theme('dashboard_widget_list_item', array('title' => $value['title'], 'id' => $id)),
          'class' => array('dashboard-column'),
      );
    } 
    else {
      $groups[$value['group']] = array(
          'data' => '<span class="widget-group-name">' . $value['group'] . '</span>',
          'class' => array('widget-group-wrapper'),
          'children' => array(
              array(
                  'data' => theme('dashboard_widget_list_item', array('title' => $value['title'], 'id' => $id)),
                  'class' => array('dashboard-column'),
              ),
          )
      );
    }
  }  
  return theme('item_list', array('items' => $groups));
}

/**
 * Theme: Draggable widget.
 */
function theme_dashboard_widget($vars) {
  $output = '';
  $output .= '<div class="widget" data-id="' . check_plain($vars['id']) . '">';
  $output .= '<h2>' . check_plain($vars['title']) . '</h2>';
  $output .= '<div class="widget-description">';
  $output .= check_plain($vars['description']);
  $output .= '</div>';
  $output .= '<div class="widget-content">';
  $output .= $vars['content'];
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Theme: Dashboard widget list item.
 */
function theme_dashboard_widget_list_item($vars) {
  $output = '';
  $output .= '<div class="widget not-processed" data-id="'. check_plain($vars['id']) .'">';
  $output .= '<h2>' . check_plain($vars['title']) . '</h2>';
  $output .= '</div>';

  return $output;
}

/**
 * Theme: Dashboard layout builder.
 */
function theme_dashboard_layout($vars) {
  $output = '';
  $count = 1;
 
  foreach ((array) $vars['setup'] as $class) {
    $output .= '<div class="dashboard-column dashboard-column-' . check_plain($class) . '" data-id="column-index-' . $count . '">';
    $output .= isset($vars['regions']['column-index-' . $count])? $vars['regions']['column-index-' . $count] : '';
    $output .= '</div>';
    $count ++;
  }
  
  return $output;
}
